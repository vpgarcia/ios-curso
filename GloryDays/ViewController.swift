//
//  ViewController.swift
//  GloryDays
//
//  Created by Vicente Pérez-Olivares García on 22/11/16.
//  Copyright © 2016 iwacom. All rights reserved.
//

import UIKit
import AVFoundation
import Photos
import Speech

class ViewController: UIViewController {

    @IBOutlet var infoLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


    @IBAction func askForPermissions(_ sender: UIButton) {
        self.askForPhotoPermissions()
    }
    
    func askForPhotoPermissions(){
        PHPhotoLibrary.requestAuthorization { [unowned self](authStatus) in
            
            //Se manda la ejecución del hilo principal
            //para que el bloque de código se ejecute en el hilo principal
            //Para que los permisos se acepten o se denieguen en tiempo real
            DispatchQueue.main.async {
                
                if authStatus == .authorized {
                    self.askForRecordPermissions()
                    
                }else {
                    self.infoLabel.text = "Has denegado el permiso de fotos. Por favor actívalo manualmente en Ajustes."
                }
            }
            
            
        }
    }
    
    func askForRecordPermissions(){
        
        AVAudioSession.sharedInstance().requestRecordPermission { [unowned self] (allowed) in
            DispatchQueue.main.async {
                
                if allowed {
                    self.askForTranscriptionPermissions()
                }else {
                    self.infoLabel.text = "Has denegado el permiso de grabación. Por favor actívalo manualmente en Ajustes."
                }
            }
        }
        
    }
    
    func askForTranscriptionPermissions(){
        SFSpeechRecognizer.requestAuthorization { [unowned self](authStatus) in
            DispatchQueue.main.async {
                
                if authStatus == .authorized {
                    self.authorizationCompleted()
                    
                }else {
                    self.infoLabel.text = "Has denegado el permiso de transcripción. Por favor actívalo manualmente en Ajustes."
                }
            }
        }
    }
    
    func authorizationCompleted(){
        //Oculta el viewController
        self.dismiss(animated: true, completion: nil)
    }
    
}

