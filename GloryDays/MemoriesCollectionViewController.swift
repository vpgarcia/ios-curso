//
//  MemoriesCollectionViewController.swift
//  GloryDays
//
//  Created by Vicente Pérez-Olivares García on 23/11/16.
//  Copyright © 2016 iwacom. All rights reserved.
//

import UIKit
import AVFoundation
import Photos
import Speech


private let reuseIdentifier = "cell"

class MemoriesCollectionViewController: UICollectionViewController ,UIImagePickerControllerDelegate,UINavigationControllerDelegate {
    
    //Array de strings (paths) de archivos
    var memories: [URL] = []

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.loadMemories()
        
        //Añadir boton por código
        
        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector (self.addImagePressed))

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Register cell classes
        //self.collectionView!.register(UICollectionViewCell.self, forCellWithReuseIdentifier: reuseIdentifier)

        // Do any additional setup after loading the view.
    }
    
    //Buena práctica
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.checkForGrantedPermissions()
    }
    
    func checkForGrantedPermissions(){
        let photoAuth :Bool = PHPhotoLibrary.authorizationStatus() == .authorized
        let recordingAuth :Bool = AVAudioSession.sharedInstance().recordPermission() == .granted
        let transcriptionAuth: Bool = SFSpeechRecognizer.authorizationStatus() == .authorized
        
        let authorized: Bool = photoAuth && recordingAuth && transcriptionAuth
        
        if !authorized {
            
            if let vc = storyboard?.instantiateViewController(withIdentifier: "ShowTerms"){
                navigationController?.present(vc,animated: true)
            }
        }
    }
    
    func loadMemories(){
        self.memories.removeAll()
        
        guard let files = try? FileManager.default.contentsOfDirectory(at: getDocumentsDirectory(), includingPropertiesForKeys: nil, options: []) else {
            return
        }
        
        //Si no ha fallado continúa por aquí
        
        for file in files {
            guard let fileName = try? file.lastPathComponent else {continue}
            
            if fileName.hasSuffix(".thumb"){
                let noExtension = fileName.replacingOccurrences(of: ".thumb", with: "")
                
                if let memoryPath = try? getDocumentsDirectory().appendingPathComponent(noExtension) {
                    memories.append(memoryPath)
                }
                
            }
        }
        //La sección 0 es la caja de búsqueda
        collectionView?.reloadSections(IndexSet(integer:1))
    }
    
    func getDocumentsDirectory()-> URL {
        
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        let documentsDirectory = paths[0]
        
        return documentsDirectory
    }
    
    func addImagePressed(){
        let vc = UIImagePickerController()
        
        vc.modalPresentationStyle = .formSheet
        vc.delegate = self
        navigationController?.present(vc,animated: true)
    }
    
    //Cuando el usuario ha elegido una imagen tras pulsar en el botón añadir
    //func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
        
        if let theImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            addNewMemory(image: theImage)
            self.loadMemories()
        }
        self.dismiss(animated: true, completion: nil)
    }
    
    func addNewMemory(image:UIImage){
        let memoryName = "memory-\(Date().timeIntervalSince1970)"
        
        let imageName = "\(memoryName).jpg"
        let thumbName = "\(memoryName).thumb"
        
        do {
            let imagePath = try getDocumentsDirectory().appendingPathComponent(imageName)
            
            if let jpegData = UIImageJPEGRepresentation(image, 80){
                
                try jpegData.write(to: imagePath, options: [.atomicWrite])
                
            }
            
            if let thumbnail = resizeImage(image: image, to: 200) {
                
                let thumbPath = try getDocumentsDirectory().appendingPathComponent(thumbName)
                
                if let thumbData = UIImageJPEGRepresentation(thumbnail, 80){
                    try thumbData.write(to: thumbPath, options: [.atomicWrite])
                }
            }
            
        }catch {
            print("Ha fallado la escritura en disco")
        }
        
    }
    
    func resizeImage(image:UIImage,to width:CGFloat)->UIImage? {
        let scaleFactor = width/image.size.width
        let height = image.size.height * scaleFactor
        
        //Bloque donde se redibuja la imagen
        UIGraphicsBeginImageContextWithOptions(CGSize(width:width,height:height), false, 0)
        
        image.draw(in: CGRect(x:0,y:0,width:width,height:height))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        
        UIGraphicsEndImageContext()
        //Fin zona donde se redibuja la imagen
        
        return newImage
    }
    
    func imageURL(for memory:URL)->URL{
        return try! memory.appendingPathExtension("jpg")
    }
    
    func thumbnailURL(for memory:URL)->URL{
        return try! memory.appendingPathExtension("thumb")
    }
    
    func audioURL(for memory:URL)->URL{
        return try! memory.appendingPathExtension("m4a")
    }
    
    func transcriptionURL(for memory:URL)->URL{
        return try! memory.appendingPathExtension("txt")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using [segue destinationViewController].
        // Pass the selected object to the new view controller.
    }
    */

    // MARK: UICollectionViewDataSource

    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 2
    }


    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of items
        if section == 0 {
            return 0
        }else {
            return self.memories.count
        }
    }

    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as! MemoryCell
    
        // Configure the cell
        let memory = memories[indexPath.row]
        
        //Si es null lo asigna a string vacío con la sintaxis ??
        let memoryName = self.thumbnailURL(for: memory).path
        
        let image = UIImage(contentsOfFile: memoryName)
        cell.imageView.image = image
    
        return cell
    }
    
    //Para la barra de búsqueda
    override func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        let header = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "header", for: indexPath)
        return header
    }
    
    func collectionView(_ collectionView:UICollectionView,layout collectionViewLayout:UICollectionViewLayout,referenceSizeForHeaderInSection section:Int)->CGSize{
        
        if section == 0{
            return CGSize(width: 0,height:50)
        }else {
            return CGSize.zero
        }

        
    }

    // MARK: UICollectionViewDelegate

    /*
    // Uncomment this method to specify if the specified item should be highlighted during tracking
    override func collectionView(_ collectionView: UICollectionView, shouldHighlightItemAt indexPath: IndexPath) -> Bool {
        return true
    }
    */

    /*
    // Uncomment this method to specify if the specified item should be selected
    override func collectionView(_ collectionView: UICollectionView, shouldSelectItemAt indexPath: IndexPath) -> Bool {
        return true
    }
    */

    /*
    // Uncomment these methods to specify if an action menu should be displayed for the specified item, and react to actions performed on the item
    override func collectionView(_ collectionView: UICollectionView, shouldShowMenuForItemAt indexPath: IndexPath) -> Bool {
        return false
    }

    override func collectionView(_ collectionView: UICollectionView, canPerformAction action: Selector, forItemAt indexPath: IndexPath, withSender sender: Any?) -> Bool {
        return false
    }

    override func collectionView(_ collectionView: UICollectionView, performAction action: Selector, forItemAt indexPath: IndexPath, withSender sender: Any?) {
    
    }
    */

}
