//
//  MemoryCell.swift
//  GloryDays
//
//  Created by Vicente Pérez-Olivares García on 23/11/16.
//  Copyright © 2016 iwacom. All rights reserved.
//

import UIKit

class MemoryCell: UICollectionViewCell {
    @IBOutlet var imageView: UIImageView!
    
}
